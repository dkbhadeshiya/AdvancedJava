
# Advanced Java
--------------------------------------------------
This repository contains the Advanced Java lab works and some other practising projects that I've done for learning various concepts of Advanced Java.

Mostly J2EE and Core Java projects.

--------------------------------------------------

# Current Projects

|No| Name | Faculty | Description | 
|---|---------------------------|---------|----------------------------------------------------------------------------------| 
|1.| Basic Database Connection | JBT | Initial Database Connection to Oracle Database using OJDBC Drivers | 
|2.| BMI Calculator | NJG | BMI (Body Mass Index) calculator for demonstrating the use of include and forward | 
|3.| Practice Project | NJG | A Practice Project to demonstrate the use of Java Beans classes & jsp useBean tag |
