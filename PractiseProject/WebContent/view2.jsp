<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>View2</title>
</head>
<body>
	<jsp:useBean id="employee" class="dkbhadeshiya.EmployeeBeans">
		<jsp:setProperty name="employee" property="*"/>  
	</jsp:useBean>
	<h1>View Employee Details :</h1>
	<table>
		<tr>
			<td>Employee ID :</td>
			<td><jsp:getProperty property="e_id" name="employee"/></td>
		</tr>
		<tr>
			<td>Employee Name :</td>
			<td><jsp:getProperty property="name" name="employee"/></td>
		</tr>
		<tr>
			<td>Employee Surname :</td>
			<td><jsp:getProperty property="surname" name="employee"/></td>
		</tr>
		<tr>
			<td>Employee Address :</td>
			<td><jsp:getProperty property="address" name="employee"/></td>
		</tr>
		<tr>
			<td>Employee City :</td>
			<td><jsp:getProperty property="city" name="employee"/></td>
		</tr>
	</table>
</body>
</html>