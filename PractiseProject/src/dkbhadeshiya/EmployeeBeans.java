package dkbhadeshiya;

public class EmployeeBeans extends PersonBeans {
	static int count=0;
	int e_id;

	public EmployeeBeans(){
		e_id= ++count;
	}
	public static int getCount() {
		return count;
	}

	public static void setCount(int count) {
		EmployeeBeans.count = count;
	}

	public int getE_id() {
		return e_id;
	}

	public void setE_id(int e_id) {
		this.e_id = e_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
}
