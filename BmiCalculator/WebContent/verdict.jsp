<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Verdict</title>
</head>
<body>
	<% server.PersonDetails p = (server.PersonDetails) request.getAttribute("person");
		if(p.getBmi() < 20) { 
			out.print("Forwarding to UnderWeight...");
			RequestDispatcher rd = request.getRequestDispatcher("underweight.jsp");
			request.setAttribute("person", p );
			rd.forward(request,response);
		}
		if(p.getBmi() < 20) {
			out.print("Forwarding to OverWeight...");
			RequestDispatcher rd = request.getRequestDispatcher("overweight.jsp");
			request.setAttribute("person", p );
			rd.forward(request,response);
		} 
	
		if(p.getBmi() >20 && p.getBmi() <= 25) { 
			RequestDispatcher rd = request.getRequestDispatcher("normal.jsp");
			request.setAttribute("person", p );
			rd.include(request,response);
		}
	%>
</body>
</html>