<%@page import="server.PersonDetails"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>BMI Calculator</title>
</head>
<body>
	<h1>Calculating BMI...</h1>
	<%
		PersonDetails p = new PersonDetails();
		p.setName(request.getParameter("name"));
		p.setAge(Integer.parseInt(request.getParameter("age")));
		p.setWeight(Double.parseDouble(request.getParameter("weight")));
		p.setHeight(Double.parseDouble(request.getParameter("height")));
		
		p.setBmi((p.getWeight() / (p.getHeight() * p.getHeight())));	//Calculating and setting of the BMI using KG and M
		
		request.setAttribute("person", p);	//Setting the attribute to request
		

		RequestDispatcher rd = request.getRequestDispatcher("verdict.jsp");	//Creating Request Despatcher
		rd.forward(request, response);
	%>
</body>
</html>