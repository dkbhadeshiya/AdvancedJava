<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Normal BMI</title>
</head>
<body>
	<h1>Congratulations, Your BMI is Normal</h1>
	<h3>Info :</h3>
	<br>
	<% server.PersonDetails p = (server.PersonDetails) request.getAttribute("person"); %>
	<table>
		<tr>
			<td>Name :</td>
			<td><%= p.getName() %></td>
		</tr>
		<tr>
			<td>Age :</td>
			<td><%= p.getAge() %></td>
		</tr>
		<tr>
			<td>Weight :</td>
			<td><%= p.getWeight() %></td>
		</tr>
		<tr>
			<td>Height :</td>
			<td><%= p.getHeight() %></td>
		</tr>
		<tr>
			<td>BMI :</td>
			<td><%= p.getBmi() %></td>
		</tr>
	</table>
</body>
</html>