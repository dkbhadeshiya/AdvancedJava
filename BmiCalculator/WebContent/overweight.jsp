<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Over Wright BMI</title>
</head>
<body>
	<h1>Oops, Sorry!! You are Over Weight!!</h1>
	<h3>Info :</h3>
	<br>
	<% server.PersonDetails p = (server.PersonDetails) request.getAttribute("person"); %>
	<table>
		<tr>
			<td>Name :</td>
			<td><%= p.getName() %></td>
		</tr>
		<tr>
			<td>Age :</td>
			<td><%= p.getAge() %></td>
		</tr>
		<tr>
			<td>Weight :</td>
			<td><%= p.getWeight() %></td>
		</tr>
		<tr>
			<td>Height :</td>
			<td><%= p.getHeight() %></td>
		</tr>
		<tr>
			<td>BMI :</td>
			<td><%= p.getBmi() %></td>
		</tr>
	</table>
</body>
</html>
