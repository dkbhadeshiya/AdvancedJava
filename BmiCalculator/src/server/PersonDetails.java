/**
 * 
 */
package server;

import java.io.Serializable;

/**
 * @author dkbhadeshiya
 *
 */
public class PersonDetails implements Serializable {
	/**
	 * 
	 */
	/**
	 * A Bean Class to store the Data about person
	 */
	String name;
	int age;
	double weight,height,bmi;

	
	//Getter and Setter methods 
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public double getBmi() {
		return bmi;
	}
	public void setBmi(double bmi) {
		this.bmi = bmi;
	}
	
}
