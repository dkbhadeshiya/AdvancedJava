package jdbc;


import java.sql.*;

public class PreparedStatement {
	public static void main(String[] args){
		JSONConfig config = new JSONConfig("/home/dkbhadeshiya/workspace/dbconfig.json");
		try{
			Class.forName("org.mariadb.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mariadb://localhost:3306/hr",config.getUsername(),config.getPassword());
			java.sql.PreparedStatement prep = con.prepareStatement("select * from student where e_no=?");
			prep.setInt(1, 1);
			ResultSet rs = prep.executeQuery();
			while (rs.next()){
				System.out.println(rs.getString(1) + "    " + rs.getString(2) + "    "+ rs.getString(3));
			}
		}
		
		catch(Exception e){
			e.printStackTrace();
		}
	}
}
