package jdbc;

import java.sql.*;


public class MariaDB {
	
	public static void main(String arg[]){
		JSONConfig config = new JSONConfig("/home/dkbhadeshiya/workspace/dbconfig.json");
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mariadb://localhost:3306/hr",config.getUsername(),config.getPassword());
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("select * from student");
			while(rs.next()){
				System.out.println(rs.getString(1) + "    " + rs.getString(2) + "    "+ rs.getString(3));
			}
			st.close();
			con.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

}
