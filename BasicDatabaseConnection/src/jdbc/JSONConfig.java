package jdbc;
import java.io.FileReader;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class JSONConfig {
	String username,password;

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}
	public JSONConfig(String filePath) {
		JSONParser parser = new JSONParser();
		try {
				JSONObject jobj = (JSONObject) parser.parse(new FileReader(filePath));
				username = (String) jobj.get("username");
				password = (String) jobj.get("password");
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
}
